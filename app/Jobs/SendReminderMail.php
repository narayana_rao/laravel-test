<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Mail;
use App\Mail\userCreated as MailerUserCreated;

class SendReminderMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $strText;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($p_strText)
    {
        //
        $this->strText = $p_strText;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Mail::to('nrpsarla@gmail.com')->send(new MailerUserCreated('test data' . $this->strText));
    }
}
