<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event\UserCreated;

use Mail;
use App\Mail\UserCreated as MailerUserCreated;

use App\Jobs\SendReminderMail;

class UserAuth extends Controller
{
    //
    public function index() {
        // call an event
        //event(new UserCreated('Your Account has been send'));

        // for run this need to run queue worker -> php artisan queue:work
        SendReminderMail::dispatch('job test text')->delay(now()->addMinutes(1));;

        echo "mail sent";
    }

    public function mail() {
        // sent mail using mailable
        Mail::to('nrpsarla@gmail.com')->send(new MailerUserCreated('test data'));
    }
}
