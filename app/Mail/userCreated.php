<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class userCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $strText;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($p_strText)
    {
        //
        $this->strText = $p_strText;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('user@mail.com')->subject("subject is " . $this->strText)->view('welcome');
    }
}
