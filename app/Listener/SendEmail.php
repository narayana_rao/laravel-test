<?php

namespace App\Listener;

use App\Event\UserCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use App\Mail\UserCreated as mailUserCreated;

class SendEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserCreated $event)
    {
        
        Mail::to('nrpsarla@gmail.com')->send(new mailUserCreated('test data & Evnet Data ' . $event->email));
        //
        echo $event->email;
    }
}
